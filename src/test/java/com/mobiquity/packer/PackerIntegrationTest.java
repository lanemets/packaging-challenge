package com.mobiquity.packer;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import com.google.common.io.Resources;
import org.junit.jupiter.api.Test;

public class PackerIntegrationTest {

  @Test
  void pack() {
    String packed = Packer.pack(Resources.getResource("test").getPath());
    assertThat(
        packed,
        is("4\n"
            + "-\n"
            + "2,7\n"
            + "8,9"
        )
    );
  }

}
