package com.mobiquity.packer;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import com.google.common.collect.ImmutableList;
import java.math.BigDecimal;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class PackageTest {

  @ParameterizedTest
  @MethodSource("packMethodSource")
  void pack(
      int capacity,
      ImmutableList<Item> items,
      Package expectedPackage
  ) {
    assertThat(new PackAction(items, capacity).pack(), is(expectedPackage));
  }

  private static Stream<Arguments> packMethodSource() {
    return Stream.of(
        Arguments.of(
            9,
            ImmutableList.of(),
            new Package(9, ImmutableList.of())
        ),
        Arguments.of(
            81,
            ImmutableList.of(
                new Item(1, 53.38, new BigDecimal(45)),
                new Item(2, 88.62, new BigDecimal(98)),
                new Item(3, 78.48, new BigDecimal(3)),
                new Item(4, 72.30, new BigDecimal(76)),
                new Item(5, 30.18, new BigDecimal(9)),
                new Item(6, 46.34, new BigDecimal(48))
            ),
            new Package(
                81,
                ImmutableList.of(
                    new Item(4, 72.30, new BigDecimal(76))
                )
            )
        ),
        Arguments.of(
            8,
            ImmutableList.of(
                new Item(1, 15.3, new BigDecimal(34))
            ),
            new Package(8, ImmutableList.of())
        ),
        Arguments.of(
            75,
            ImmutableList.of(
                new Item(1, 85.31, new BigDecimal(29)),
                new Item(2, 14.55, new BigDecimal(74)),
                new Item(3, 3.98, new BigDecimal(16)),
                new Item(4, 26.24, new BigDecimal(55)),
                new Item(5, 63.69, new BigDecimal(52)),
                new Item(6, 76.25, new BigDecimal(75)),
                new Item(7, 60.02, new BigDecimal(74)),
                new Item(8, 93.18, new BigDecimal(35)),
                new Item(9, 89.95, new BigDecimal(78))
            ),
            new Package(
                75,
                ImmutableList.of(
                    new Item(2, 14.55, new BigDecimal(74)),
                    new Item(7, 60.02, new BigDecimal(74))
                )
            )
        ),
        Arguments.of(
            56,
            ImmutableList.of(
                new Item(1, 90.72, new BigDecimal(13)),
                new Item(2, 33.80, new BigDecimal(40)),
                new Item(3, 43.15, new BigDecimal(10)),
                new Item(4, 37.97, new BigDecimal(16)),
                new Item(5, 46.81, new BigDecimal(36)),
                new Item(6, 48.77, new BigDecimal(79)),
                new Item(7, 81.80, new BigDecimal(45)),
                new Item(8, 19.36, new BigDecimal(79)),
                new Item(9, 6.76, new BigDecimal(64))
            ),
            new Package(
                56,
                ImmutableList.of(
                    new Item(8, 19.36, new BigDecimal(79)),
                    new Item(9, 6.76, new BigDecimal(64))
                )
            )
        )
    );
  }
}