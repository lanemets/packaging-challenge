package com.mobiquity.packer;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.google.common.collect.ImmutableSet;
import com.mobiquity.packer.exceptions.FileProcessingException;
import com.mobiquity.packer.parser.LineParsed;
import com.mobiquity.packer.parser.LineParser;
import java.math.BigDecimal;
import java.util.stream.Stream;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class LineParserTest {

  @ParameterizedTest
  @MethodSource("parseSource")
  void parse(String line, LineParsed expected) {
    assertThat(LineParser.parse(line), is(expected));
  }

  @ParameterizedTest
  @MethodSource("parseWithExceptionSource")
  void parseWithExceptions(String line, Class<? extends RuntimeException> expectedType) {
    Executable closureWithException = () -> LineParser.parse(line);
    assertThrows(expectedType, closureWithException);
  }

  private static Stream<Arguments> parseWithExceptionSource() {
    return Stream.of(
        Arguments.of(
            "abc",
            FileProcessingException.class
        ),
        Arguments.of(
            "",
            FileProcessingException.class
        ),
        Arguments.of(
            "81 : (1,a53.38,€45)",
            FileProcessingException.class
        ),
        Arguments.of(
            "101 : (1,53.38,€45)",
            IllegalStateException.class
        ),
        Arguments.of(
            "10 : (1,105.38,€45)",
            IllegalStateException.class
        ),
        Arguments.of(
            "10 : (1,5.38,€101)",
            IllegalStateException.class
        ),
        Arguments.of(
            "81 : (1,53.38,€45) (2,88.62,€98) (1,53.38,€45) (2,88.62,€98) (1,53.38,€45) (2,88.62,€98)"
                + "(1,53.38,€45) (2,88.62,€98) (1,53.38,€45) (2,88.62,€98) (1,53.38,€45) (2,88.62,€98) "
                + "(1,53.38,€45) (2,88.62,€98) (1,53.38,€45) (2,88.62,€98) (1,53.38,€45) (2,88.62,€98) "
                + "(1,53.38,€45) (2,88.62,€98) (1,53.38,€45) (2,88.62,€98) (1,53.38,€45) (2,88.62,€98) ",
            IllegalStateException.class
        )
    );
  }

  private static Stream<Arguments> parseSource() {
    return Stream.of(
        Arguments.of(
            "81 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)",
            new LineParsed(
                81,
                ImmutableSet.of(
                    new Item(1, 53.38, new BigDecimal(45)),
                    new Item(2, 88.62, new BigDecimal(98)),
                    new Item(3, 78.48, new BigDecimal(3)),
                    new Item(4, 72.30, new BigDecimal(76)),
                    new Item(5, 30.18, new BigDecimal(9)),
                    new Item(6, 46.34, new BigDecimal(48))
                )
            )
        ),
        Arguments.of(
            "8 : (1,15.3,€34)",
            new LineParsed(
                8,
                ImmutableSet.of(new Item(1, 15.3, new BigDecimal(34)))
            )
        )
    );
  }
}
