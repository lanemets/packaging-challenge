package com.mobiquity.packer;

import static com.google.common.collect.ImmutableList.toImmutableList;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import java.util.StringJoiner;

public class Package {

  private final int capacity;
  private final ImmutableList<Item> items;

  public Package(int capacity, ImmutableList<Item> items) {
    this.capacity = capacity;
    this.items = items;
  }

  public ImmutableList<Integer> itemIds() {
    return items.stream()
        .map(Item::getId)
        .collect(toImmutableList());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Package aPackage = (Package) o;
    return capacity == aPackage.capacity &&
        Objects.equal(items, aPackage.items);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(capacity, items);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", Package.class.getSimpleName() + "[", "]")
        .add("capacity=" + capacity)
        .add("items=" + items)
        .toString();
  }
}
