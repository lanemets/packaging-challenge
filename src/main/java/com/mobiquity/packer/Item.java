package com.mobiquity.packer;

import static com.mobiquity.packer.Consts.WEIGHT_MULTIPLIER;

import com.google.common.base.Objects;
import java.math.BigDecimal;
import java.util.StringJoiner;

public class Item {

  private final int id;
  private final double weight;
  private final BigDecimal price;
  private final int weightMultiplied;

  public Item(int id, double weight, BigDecimal price) {
    this.id = id;
    this.weight = weight;
    this.weightMultiplied = (int) (weight * WEIGHT_MULTIPLIER);
    this.price = price;
  }

  public int getId() {
    return id;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public int getWeightMultiplied() {
    return weightMultiplied;
  }

  public double getWeight() {
    return weight;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Item item = (Item) o;
    return id == item.id &&
        Double.compare(item.weight, weight) == 0 &&
        weightMultiplied == item.weightMultiplied &&
        Objects.equal(price, item.price);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id, weight, price, weightMultiplied);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", Item.class.getSimpleName() + "[", "]")
        .add("id=" + id)
        .add("weight=" + weight)
        .add("price=" + price)
        .add("weightMultiplied=" + weightMultiplied)
        .toString();
  }
}
