package com.mobiquity.packer.parser;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableSet;
import com.mobiquity.packer.Item;
import java.util.StringJoiner;

public class LineParsed {

  private final int weightLimit;
  private final ImmutableSet<Item> items;

  public LineParsed(
      int weightLimit,
      ImmutableSet<Item> items
  ) {
    this.weightLimit = weightLimit;
    this.items = items;
  }

  public int getWeightLimit() {
    return weightLimit;
  }

  public ImmutableSet<Item> getItems() {
    return items;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", LineParsed.class.getSimpleName() + "[", "]")
        .add("weightLimit=" + weightLimit)
        .add("items=" + items)
        .toString();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LineParsed that = (LineParsed) o;
    return weightLimit == that.weightLimit &&
        Objects.equal(items, that.items);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(weightLimit, items);
  }
}
