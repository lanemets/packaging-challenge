package com.mobiquity.packer.parser;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSet.Builder;
import com.mobiquity.packer.Item;
import com.mobiquity.packer.exceptions.FileProcessingException;
import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LineParser {

  private static final Pattern ITEM_LINE_PATTERN = Pattern.compile(
      "\\((?<id>\\d+),(?<weight>\\d+(\\.\\d{1,2})?),€(?<price>\\d+(\\.\\d{1,2})?)\\)");

  private static final Pattern ITEM_LINE_VALIDATION_PATTERN = Pattern.compile(
      "(?<weightLimit>\\d{1,3})\\s+:\\s+((\\(((?<id>\\d+),(?<weight>\\d+(\\.\\d{1,2})?),€(?<price>\\d+(\\.\\d{1,2})?))\\)\\s*)+)"
  );

  /***
   * Parses and validates the line given to LineParsed
   * @param line string from input file as-is
   * @return an object with all items and capacity parsed
   */
  public static LineParsed parse(String line) {
    Matcher lineMatcher = ITEM_LINE_VALIDATION_PATTERN.matcher(line);
    if (!lineMatcher.matches()) {
      throw new FileProcessingException(String
          .format("Error on file processing; incorrect line format; line: %s", line)
      );
    }

    int weightLimit = Integer.parseInt(lineMatcher.group("weightLimit"));

    Preconditions.checkState(
        weightLimit <= 100,
        "Max weight that a package can take should be ≤ 100"
    );

    Matcher itemLineMatcher = ITEM_LINE_PATTERN.matcher(line);

    Builder<Item> itemsBuilder = ImmutableSet.builder();
    int itemsCounter = 0;

    while (itemLineMatcher.find()) {
      int id = Integer.parseInt(itemLineMatcher.group("id"));
      double weight = Double.parseDouble(itemLineMatcher.group("weight"));
      Preconditions.checkState(
          weight <= 100,
          "Max price of an item should be ≤ 100"
      );

      BigDecimal price = new BigDecimal(itemLineMatcher.group("price"));

      Preconditions.checkState(
          price.compareTo(new BigDecimal(100)) <= 0,
          "Max price of an item should be ≤ 100"
      );

      itemsBuilder.add(new Item(id, weight, price));
      Preconditions.checkState(
          itemsCounter++ <= 15,
          "There might be up to 15 items to choose from"
      );
    }

    return new LineParsed(weightLimit, itemsBuilder.build());
  }
}
