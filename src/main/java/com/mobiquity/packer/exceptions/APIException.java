package com.mobiquity.packer.exceptions;

public class APIException extends RuntimeException {

  public APIException(String message, Throwable cause) {
    super(message, cause);
  }
}
