package com.mobiquity.packer.exceptions;

public class FileProcessingException extends RuntimeException {

  public FileProcessingException(String message) {
    super(message);
  }

}
