package com.mobiquity.packer;

import com.google.common.base.Joiner;
import com.mobiquity.packer.exceptions.APIException;
import com.mobiquity.packer.exceptions.FileProcessingException;
import com.mobiquity.packer.parser.LineParser;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Packer {

  /**
   * Packs item parsed from input file
   *
   * @param path Absolute path to file
   * @return string with packages ids filled with items
   * @throws APIException in case of data validation errors or other
   */
  public static String pack(String path) throws APIException {
    try (Stream<String> lines = Files.lines(Paths.get(path))) {
      return lines.parallel()
          .map(LineParser::parse)
          .map(line -> new PackAction(line.getItems().asList(), line.getWeightLimit()).pack())
          .map(Package::itemIds)
          .map(line -> line.size() == 0 ? "-" : Joiner.on(",").join(line))
          .collect(Collectors.joining("\n"));
    } catch (IOException e) {
      throw new APIException("Error on file reading: ", e);
    } catch (FileProcessingException e) {
      throw new APIException("Error on file processing", e);
    } catch (Exception e) {
      throw new APIException("Other error has occurred", e);
    }
  }
}
