package com.mobiquity.packer;

import static com.google.common.collect.ImmutableList.toImmutableList;
import static com.mobiquity.packer.Consts.WEIGHT_MULTIPLIER;

import com.google.common.collect.ImmutableList;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class PackAction {

  private final ImmutableList<Item> items;
  private final int weightLimit;
  private final int weightLimitMultiplied;

  public PackAction(ImmutableList<Item> items, int weightLimit) {
    this.weightLimit = weightLimit;
    this.weightLimitMultiplied = weightLimit * WEIGHT_MULTIPLIER;

    this.items = prepareItems(items);
  }

  /**
   * Packs items to package using DP (knapsack 0-1) approach with bottom-up table
   *
   * @return new Package with items packed
   */
  public Package pack() {
    if (items.size() == 0) {
      return new Package(weightLimit, ImmutableList.of());
    }

    BigDecimal[][] bottomUpTable = getBottomUpTable();
    ImmutableList<Item> itemsToPut = getItemsToPut(bottomUpTable);
    return new Package(weightLimit, itemsToPut);
  }

  /**
   * Performs some optimization (removing outweighed items) and sort items in order to fulfill the
   * requirements
   *
   * @param items items to pack
   * @return items sorted and without outweighed
   */
  private ImmutableList<Item> prepareItems(ImmutableList<Item> items) {
    return items
        .stream()
        .filter(item -> item.getWeight() < weightLimitMultiplied)
        .sorted((left, right) ->
            left.getPrice().equals(right.getPrice())
                ? Comparator.comparingInt(Item::getWeightMultiplied).compare(left, right)
                : Comparator.comparing(Item::getPrice).compare(left, right)
        )
        .collect(toImmutableList());
  }

  /**
   * Creates bottom-up table from items given
   *
   * @return table with items' prices maximized
   */
  private BigDecimal[][] getBottomUpTable() {
    BigDecimal[][] table = new BigDecimal[items.size() + 1][weightLimitMultiplied + 1];

    for (int itemsIndex = 0; itemsIndex <= weightLimitMultiplied; itemsIndex++) {
      table[0][itemsIndex] = BigDecimal.ZERO;
    }

    for (int itemsIndex = 1; itemsIndex <= items.size(); itemsIndex++) {
      for (int capacityIndex = 0; capacityIndex <= weightLimitMultiplied; capacityIndex++) {
        if (this.items.get(itemsIndex - 1).getWeightMultiplied() > capacityIndex) {
          table[itemsIndex][capacityIndex] = table[itemsIndex - 1][capacityIndex];
        } else {
          table[itemsIndex][capacityIndex] =
              table[itemsIndex - 1][capacityIndex]
                  .max(
                      table[itemsIndex - 1][capacityIndex - items.get(itemsIndex - 1)
                          .getWeightMultiplied()]
                          .add(this.items.get(itemsIndex - 1).getPrice())
                  );
        }
      }
    }
    return table;
  }

  /**
   * Traverses bottom-up matrix from the end to find elements chosen for packing
   *
   * @return items to include in pack
   */
  private ImmutableList<Item> getItemsToPut(BigDecimal[][] bottomUpTable) {
    BigDecimal currentPriceMaximized = bottomUpTable[items.size()][weightLimitMultiplied];
    int currentCapacity = weightLimitMultiplied;

    Set<Item> itemsToPut = new TreeSet<>(Comparator.comparingInt(Item::getId));
    for (int lastRowIndex = items.size();
        lastRowIndex > 0 && currentPriceMaximized.compareTo(BigDecimal.ZERO) > 0;
        lastRowIndex--
    ) {
      if (currentPriceMaximized.compareTo(bottomUpTable[lastRowIndex - 1][currentCapacity]) != 0) {
        itemsToPut.add(items.get(lastRowIndex - 1));
        currentPriceMaximized = currentPriceMaximized
            .subtract(items.get(lastRowIndex - 1).getPrice());
        currentCapacity -= items.get(lastRowIndex - 1).getWeightMultiplied();
      }
    }
    return ImmutableList.copyOf(itemsToPut);
  }
}
